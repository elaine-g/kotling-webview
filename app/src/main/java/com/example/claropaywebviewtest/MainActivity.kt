package com.example.claropaywebviewtest

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.widget.TextView
import android.webkit.WebView
import kotlinx.android.synthetic.main.activity_main.*
import com.globalkit.microapps.SDKClaroApi
import com.globalkit.microapps.appmanagement.configuration.DBHelper
import android.webkit.WebChromeClient


class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        instancesdk(applicationContext)

        buttonTestSdk.setOnClickListener{
            println("######## show WebView #########")
            exposeWebView()
        }

    }

    fun exposeWebView (): Any{
        val myWebView = findViewById<WebView>(R.id.webView)
        myWebView.getSettings().setJavaScriptEnabled(true)
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true)
        myWebView.getSettings().setDomStorageEnabled(true)
        myWebView.setWebChromeClient(WebChromeClient())

        return myWebView.loadUrl("10.0.1.168:3000")
    }

    fun showDialog () : Any {
        val text = findViewById<TextView>(R.id.textView).text
        val dialog = AlertDialog.Builder(this)
        val userInfo = SDKClaroApi.getUserInfo()
        dialog.setTitle("Test alert")
        dialog.setMessage("Hello World! $text $userInfo")
        return dialog.show()
    }

    fun instancesdk(ctx: Context){
        SDKClaroApi.initSDK(DBHelper(ctx))
    }
}
